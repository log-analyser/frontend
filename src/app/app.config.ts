/**
 * Global configuration of the application available from any place in project
 *
 * @export
 * @class AppConfig
 */
export class AppConfig {
  public static API_ENDPOINT = 'http://localhost:5000';
}
