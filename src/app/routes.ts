import { Routes } from '@angular/router';
import { HostsListComponent } from './components/hosts-list/hosts-list.component';
import { LogListComponent } from './components/log-list/log-list.component';
import { RulesListComponent } from './components/rules-list/rules-list.component';
import { RuleFormComponent } from './components/rule-form/rule-form.component';
import { HostVariablesListComponent } from './components/host-variables-list/host-variables-list.component';
import { NotificationsListComponent } from './components/notifications-list/notifications-list.component';
import { NotificationFormComponent } from './components/notification-form/notification-form.component';
import { SettingsComponent } from './components/settings/settings.component';
import { NotificationResultsComponent } from './components/notification-results/notification-results.component';

export const appRoutes: Routes = [
  { path: 'hosts', component: HostsListComponent },
  { path: 'logs', component: LogListComponent },
  { path: 'rules', component: RulesListComponent },
  { path: 'rule/new', component: RuleFormComponent },
  { path: 'host/:id/variables', component: HostVariablesListComponent },
  { path: 'notifications', component: NotificationsListComponent },
  { path: 'notifications/results', component: NotificationResultsComponent },
  { path: 'notifications/new', component: NotificationFormComponent },
  { path: 'notifications/:id', component: NotificationFormComponent },
  { path: 'settings', component: SettingsComponent },
];
