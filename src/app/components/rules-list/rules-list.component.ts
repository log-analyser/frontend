import { Component, OnInit } from '@angular/core';
import { RulesService } from '../../services/rules.service';
import { Rule } from '../../interfaces/rule';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/api';
import { ActivatedRoute } from '@angular/router';
import { RuleResponse } from '../../interfaces/rule-response';
import { RulesSet } from '../../interfaces/rules-set';

/**
 * Displaying list of rules
 *
 * @export
 * @class RulesListComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-rules-list',
  templateUrl: './rules-list.component.html',
  styleUrls: ['./rules-list.component.scss']
})
export class RulesListComponent implements OnInit {

  rule: Rule;
  hostId: String;
  rules: RulesSet[];

  selectedRuleSet: RulesSet = null;
  ruleResults: RuleResponse;

  constructor(private rulesService: RulesService, private messageService: MessageService) { }

  /**
   * Load set of rules
   *
   * @memberof RulesListComponent
   */
  ngOnInit() {
    this.rulesService.getRulesSets().subscribe(
      res => {
        this.rules = res;
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas pobierania reguł'});
      }
    );
  }

  /**
   * Set rule's set to edit
   */
  onRowSelect(event) {
    this.rulesService.setSelectedRuleSet(this.selectedRuleSet);
  }

  /**
   * Clean the shared rule data in service while creating the new one
   *
   * @memberof RulesListComponent
   */
  onAddNewRule() {
    this.rulesService.setSelectedRuleSet(null);
  }
}
