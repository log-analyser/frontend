import { Component, OnInit, Input } from '@angular/core';
import { Variable } from '../../interfaces/variable';
import { Rule } from '../../interfaces/rule';
import { RulesSet } from '../../interfaces/rules-set';
import { Host } from '../../interfaces/host';
import { HostService } from '../../services/host.service';
import { RulesService } from '../../services/rules.service';
import { MessageService } from 'primeng/components/common/messageservice';

/**
 * Displaying form to create a new rule
 *
 * @export
 * @class RuleFormComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-rule-form',
  templateUrl: './rule-form.component.html',
  styleUrls: ['./rule-form.component.scss']
})
export class RuleFormComponent implements OnInit {

  rulesSet: RulesSet = {variables: []};
  hosts: Host[];

  constructor(
    private hostService: HostService,
    private rulesService: RulesService,
    private messageService: MessageService
  ) { }

  /**
   * Init rules set data.
   * Load hosts' list.
   * Prepare data for seleted rules and transform time do display correctly by widget
   *
   * @memberof RuleFormComponent
   */
  ngOnInit() {
    this.rulesSet.variables.push({rules: [{}]});
    this.hostService.getHosts().subscribe(
      hosts => {
        this.hosts = hosts;
      },
      err => {
        console.error(err);
      }
    );

    this.rulesSet = this.rulesService.selectedRuleSet ? this.rulesService.selectedRuleSet : this.rulesSet;
    this.transformTime();
  }

  /**
   * Transform text time to Date object
   *
   * @private
   * @memberof RuleFormComponent
   */
  private transformTime() {
    const rules = this.rulesSet.variables.map(r => r.rules);
    const rules2 = [].concat.apply([], rules); // flattening array
    rules2.forEach(r => {
      r.startTime = r.startTime ? new Date(r.startTime) : null;
      r.stopTime = r.stopTime ? new Date(r.stopTime) : null;
    });
  }

  /**
   * Add new variable to set of rules
   *
   * @memberof RuleFormComponent
   */
  addVariable() {
    this.rulesSet.variables.push({rules: [{}]});
  }

  /**
   * Add new rule to the set
   *
   * @memberof RuleFormComponent
   */
  addRule() {
    this.rulesSet.variables[this.rulesSet.variables.length - 1].rules.push({});
  }

  /**
   * Removing rule from set
   *
   * @param {Variable} \Variable to remove the rule from
   * @param {Rule} rule \Rule to remove from variable
   * @memberof RuleFormComponent
   */
  deleteRule(variable: Variable, rule: Rule) {
    const index = variable.rules.indexOf(rule);
    variable.rules.splice(index, 1);
  }

  /**
   * Update the existing one rules' set or save the new one
   *
   * @memberof RuleFormComponent
   */
  save() {
    if (this.rulesService.selectedRuleSet) {
      this.rulesService.updateRulesSet(this.rulesSet).subscribe(
        rulesSet => {
          this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Zestaw reguł został poprawnie zakutalizowany'});
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas aktualizacji'});
        }
      );
    } else {
      this.rulesService.saveRulesSet(this.rulesSet).subscribe(
        rulesSet => {
          this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Zestaw reguł został poprawnie zapisany'});
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas zapisywania zestawu reguł'});
        }
      );
    }
  }

  /**
   * Delete the selected rules' set
   *
   * @memberof RuleFormComponent
   */
  delete() {
    this.rulesService.deleteRulesSet(this.rulesSet).subscribe(
      rulesSet => {
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Zestaw reguł został poprawnie usunięty'});
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas usuwania zestawu reguł'});
      }
    );
  }
}
