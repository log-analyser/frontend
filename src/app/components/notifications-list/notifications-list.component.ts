import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '../../services/notifications.service';
import { Notification } from '../../interfaces/notification';
import { MessageService } from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';

/**
 * Displaying table of existing notification
 *
 * @export
 * @class NotificationsListComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-notifications-list',
  templateUrl: './notifications-list.component.html',
  styleUrls: ['./notifications-list.component.scss']
})
export class NotificationsListComponent implements OnInit {

  notifications: Notification[];
  selectedNotification: Notification;

  constructor(private notificationsService: NotificationsService, private messageService: MessageService, private router: Router) { }

  /**
   * Load all notifications
   *
   * @memberof NotificationsListComponent
   */
  ngOnInit() {
    this.notificationsService.getAllNotifications().subscribe(
      notifications => {
        this.notifications = notifications;
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas pobierania powiadomień'});
      }
    );
  }

  /**
   * Clean selected notification data if the new one is created
   */
  onAddNewNotification() {
    this.notificationsService.selectedNotification = null;
  }

  /**
   * Set data of selected notification based on selected row
   *
   * @param {any} $event Event data
   * @memberof NotificationsListComponent
   */
  onRowSelect($event) {
    this.notificationsService.selectedNotification = this.selectedNotification;
    this.router.navigateByUrl(`/notifications/${$event.data._id}`);

  }

  /**
   * Run notification to check it for hosts
   *
   * @param {Notification} notification Notification to run
   * @memberof NotificationsListComponent
   */
  runNotification(notification: Notification) {
    this.notificationsService.runNotification(notification).subscribe(
      n => {
        notification = n;
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Powiadomienie zostało uruchomione'});
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Powiadomienie nie zostało uruchomione'});
      }
    );
  }

  /**
   * Start notification to run in a schedule
   *
   * @param {Notification} notification Notification to schedule
   * @memberof NotificationsListComponent
   */
  scheduleNotification(notification: Notification) {
    this.notificationsService.scheduleNotification(notification).subscribe(
      n => {
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Powiadomienie będzie uruchamiane zgodnie z harmonogramem'});
        Object.assign(notification, n);
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Powiadomienie nie zostało uruchomione'});

      }
    );
  }

  /**
   * Stop notification schedule working
   *
   * @param {Notification} notification Notification to stop
   * @memberof NotificationsListComponent
   */
  stopNotification(notification: Notification) {
    this.notificationsService.stopNotification(notification).subscribe(
      n => {
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Powiadomienie zostało zatrzymane'});
        Object.assign(notification, n);
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Powiadomienie nie zostało zatrzymane'});

      }
    );
  }

  /**
   * Remove notification from databse
   *
   * @param {Notification} notification Notification to remove
   * @memberof NotificationsListComponent
   */
  removeNotification(notification: Notification) {
    this.notificationsService.deleteNotification(notification).subscribe(
      n => {
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Powiadomienie zostało usunięte'});
        this.notifications.splice(this.notifications.indexOf(notification), 1);
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas usuwania powiadomienia'});
      }
    );
  }
}
