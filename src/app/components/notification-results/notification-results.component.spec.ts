import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationResultsComponent } from './notification-results.component';

describe('NotificationResultsComponent', () => {
  let component: NotificationResultsComponent;
  let fixture: ComponentFixture<NotificationResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
