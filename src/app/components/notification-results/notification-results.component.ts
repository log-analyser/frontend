import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '../../services/notifications.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { Notification } from '../../interfaces/notification';

/**
 * Display results of found rules in logs
 *
 * @export
 * @class NotificationResultsComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-notification-results',
  templateUrl: './notification-results.component.html',
  styleUrls: ['./notification-results.component.scss']
})
export class NotificationResultsComponent implements OnInit {

  notificationsResults: Notification[];
  rowGroupMetadata: any;

  constructor(private notificationsService: NotificationsService, private messageService: MessageService) { }

  /**
   * Load all notifications
   *
   * @memberof NotificationResultsComponent
   */
  ngOnInit() {
    this.notificationsService.getAllNotifications().subscribe(
      notifications => {
        this.notificationsResults = notifications;
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas pobierania powiadomień'});
      }
    );
  }

  /**
   * Delete selected result from selected notification
   *
   * @param {Notification} notification Notification to remove the result from
   * @param {number} index Index of the result to remove from notification
   * @memberof NotificationResultsComponent
   */
  deleteNotificationResult(notification: Notification, index: number) {
    const notificationCopy: Notification = {... notification};
    notificationCopy.files.splice(index, 1);
    this.notificationsService.updateNotification(notificationCopy).subscribe(
      updatedNotification => {
        notification = updatedNotification;
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Wynik zostawł usunięty'});
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas usuwania wyniku'});
      }
    );
  }

}
