import { Component, OnInit } from '@angular/core';
import { HostService } from '../../services/host.service';
import { Host } from '../../interfaces/host';
import { MessageService } from 'primeng/components/common/messageservice';
import { Notification } from '../../interfaces/notification';
import { Variable } from '../../interfaces/variable';
import { SelectItem } from 'primeng/components/common/api';
import { HostVariable } from '../../interfaces/host-variable';
import { NotificationVariable } from '../../interfaces/notification-variable';
import { NotificationsService } from '../../services/notifications.service';
import { ActivatedRoute } from '@angular/router';
import { RulesSet } from '../../interfaces/rules-set';
import { RulesService } from '../../services/rules.service';

/**
 * Displaying form to create a new notification
 *
 * @export
 * @class NotificationFormComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-notification-form',
  templateUrl: './notification-form.component.html',
  styleUrls: ['./notification-form.component.scss']
})
export class NotificationFormComponent implements OnInit {

  hosts: Host[];
  notification: Notification = {rulesSets: [], requiredVariablesSets: []};
  rulesSets: RulesSet[];
  nextVariables: NotificationVariable[][] = [];
  availableVariables: NotificationVariable[];

  /**
   * Array of all variables in notification with clean values
   *
   * @type {NotificationVariable[]}
   * @memberof NotificationFormComponent
   */
  cleanVariablesSet: NotificationVariable[];

  constructor(
    private notificationsService: NotificationsService,
    private hostService: HostService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private rulesService: RulesService
  ) { }

  /**
   * Load hosts' list.
   * Load rules' list.
   *
   */
  ngOnInit() {
    this.hostService.getHosts().subscribe(
      hosts => {
        this.hosts = hosts;
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas pobierania hostów'});
      }
    );

    this.rulesService.getRulesSets().subscribe(
      rulesSets => {
        this.rulesSets = rulesSets;
        if (this.notificationsService.selectedNotification) {
          this.notification = this.notificationsService.selectedNotification;
          this.loadVariablesFromRulesSets();
        }

      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas pobierania zestawów reguł'});
      }
    );
  }

  /**
   * Load variables that can be set as a next variable
   *
   * @memberof NotificationFormComponent
   */
  loadNextVariables() {
    if (this.cleanVariablesSet) {
      this.nextVariables = [];
      this.cleanVariablesSet.forEach((v, index) => {
        this.nextVariables.push(
          this.cleanVariablesSet.filter(v2 => v2._id !== v._id).map(v2 => ({name: v2.name})));
      });
    }
  }

  /**
   * Load all variables from rules added to notification
   *
   * @memberof NotificationFormComponent
   */
  loadVariablesFromRulesSets() {
    this.cleanVariablesSet = [];
    let variableSet = this.notification.rulesSets.map(rulesSet => rulesSet.variables.map(v => ({name: v.name, value: '', _id: v._id})));
    variableSet = [].concat.apply([], variableSet);
    this.cleanVariablesSet = [...variableSet];
    if(!this.notificationsService.selectedNotification) {
      this.notification.requiredVariablesSets = [];
      this.addAlternativeSet();
    }

    this.loadNextVariables();
  }

  /**
   * Remove variable from set
   *
   * @param {any} variablesSet Set of all variables in notification
   * @param {any} index Index of the variable to remove from noification
   * @memberof NotificationFormComponent
   */
  removeVariable(variablesSet, index) {
    variablesSet = variablesSet.splice(index, 1);
  }

  /**
   * Add the next set of all variables available in notification
   *
   * @memberof NotificationFormComponent
   */
  addAlternativeSet() {
    const copy = JSON.parse(JSON.stringify(this.cleanVariablesSet));
    this.notification.requiredVariablesSets.push(copy);
  }

  /**
   * Remove the set of variables from notification
   *
   * @param {any} variableSet
   * @memberof NotificationFormComponent
   */
  removeVariableSet(variableSet) {
    const index = this.notification.requiredVariablesSets.indexOf(variableSet);
    if (index !== -1) {
      this.notification.requiredVariablesSets.splice(index, 1);
    }
  }

  /**
   * Save new notification data or update the existing one
   *
   * @memberof NotificationFormComponent
   */
  save() {
    if (this.notificationsService.selectedNotification) {
      this.notificationsService.updateNotification(this.notification).subscribe(
        notification => {
          this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Powiadomienie zostało zaktualizowane'});
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas aktualizacji powiadomienia'});
        }
      );
    } else {
      this.notificationsService.saveNotification(this.notification).subscribe(
        notification => {
          this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Powiadomienie zostało zapisane'});
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas zapisywania powiadomienia'});
        }
      );
    }

  }

  /**
   * Remove selected notification from database
   *
   * @memberof NotificationFormComponent
   */
  delete() {
    this.notificationsService.deleteNotification(this.notification).subscribe(
      notification => {
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Powiadomienie zostało usunięte'});
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas usuwania powiadomienia'});
      }
    );
  }
}
