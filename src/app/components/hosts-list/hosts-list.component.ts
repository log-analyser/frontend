import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { HostService } from '../../services/host.service';
import { Host } from '../../interfaces/host';
import {Message, SelectItem} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { RulesService } from '../../services/rules.service';
import { RuleResponse } from '../../interfaces/rule-response';

/**
 * Displaying list of hosts with host's data
 *
 * @export
 * @class HostsListComponent
 * @implements {OnInit}
 * @implements {OnChanges}
 */
@Component({
  selector: 'app-hosts-list',
  templateUrl: './hosts-list.component.html',
  styleUrls: ['./hosts-list.component.scss']
})
export class HostsListComponent implements OnInit, OnChanges {


  displayDialog: boolean;
  host: Host = {};
  selectedHost: Host;
  newHost: boolean;
  hosts: Host[];
  cols: any[];
  protocols: SelectItem[];

  rulesResults: RuleResponse[];
  displayVariablesDialog: boolean;

  constructor(private hostService: HostService, private messageService: MessageService, private rulesService: RulesService) {}

  ngOnChanges(changes: SimpleChanges): void {
  }

  /**
   * Load hosts list using service.
   * Create table headers.
   * Init protocols list.
   */
  ngOnInit() {
    this.hostService.getHosts().subscribe(
      res => {
        this.hosts = <Object[]> res;
      },
      err => console.log(err)
    );

    this.cols = [
      {field: 'name', header: 'Nazwa', width: '300px'},
      {field: 'address', header: 'Adres', width: '300px'},
      {field: 'protocolType', header: 'Protokół', width: '100px'},
      {field: 'paths', header: 'Ścieżki logów'}
    ];

    this.protocols = [
      {label: 'FTP', value: 'ftp'},
      {label: 'SSH', value: 'ssh'},
      {label: 'HTTP(S)', value: 'http'},
    ];
  }

  /**
   * Check if variable is an Array
   * @param variable Variable to check
   * @return true if array, false otherwise
   */
  isArray(variable) {
    return variable instanceof Array;
  }

  /**
   * Display the dialog to add new host
   */
  showDialogToAdd() {
    this.newHost = true;
    this.host = {paths: [{}]};
    this.displayDialog = true;
  }

  /**
   * Add new inputs to add the next path for new host
   */
  addPathInput() {
    this.host.paths.push({});
  }

  /**
   * Save the new host or update existing one
   */
  save() {
    const hosts = [...this.hosts];
    if (this.newHost) {
      this.hostService.postHost(this.host).subscribe(
        res => {
          hosts.push(res);
          this.displayDialog = false;
          this.hosts = hosts;
          this.host = null;
          this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Nowy host został dodany'});
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas dodawania hosta'});
        }
      );
    } else {
      this.hostService.updateHost(this.host).subscribe(
        res => {
          hosts[this.hosts.indexOf(this.selectedHost)] = this.host;
          this.displayDialog = false;
          this.hosts = hosts;
          this.host = null;
          this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Host został zaktualizowany'});
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas aktualizacji hosta'});
        }
      );
    }
  }

  /**
   * Delete selected host form database
   */
  delete() {
    this.hostService.deleteHost(this.selectedHost._id).subscribe(
      res => {
        const index = this.hosts.indexOf(this.selectedHost);
        this.hosts = this.hosts.filter((val, i) => i !== index);
        this.host = null;
        this.displayDialog = false;
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Wybrany host został usunięty'});
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas usuwania hosta'});
      }
    );
  }

  /**
   * Set host to edit and display edit dialog
   * @param event Event data (i.e row data from table)
   */
  onRowSelect(event) {
    this.newHost = false;
    this.host = {... event.data};
    this.displayDialog = true;
  }

  trackByIndex(index: any, item: any) {
    return index;
  }

  /**
   * Remove path from the host
   * @param {string} path Path where logs are stored on host
   * @memberof HostsListComponent
   */
  removePath(path) {
    const index = this.host.paths.indexOf(path);
    this.host.paths.splice(index, 1);
  }

}
