import { Component, OnInit } from '@angular/core';
import { SettingsItem, Settings } from '../../interfaces/settings-item';
import { SettingsService } from '../../services/settings.service';
import { MessageService } from 'primeng/components/common/messageservice';

/**
 * Manage settings in the project
 *
 * @export
 * @class SettingsComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  settings: any = {email: {data: {}}};

  constructor(private settingsService: SettingsService, private messageService: MessageService) {}

  /**
   * Loading settings from database and transform it to the right structure
   *
   * @memberof SettingsComponent
   */
  ngOnInit() {
    this.settingsService.getSettings().subscribe(
      settings => {
        settings.forEach(settingsItem => {
          this.settings[settingsItem.name]._id = settingsItem._id;
          this.settings[settingsItem.name].data = settingsItem.data || {};
          this.settings[settingsItem.name].name = settingsItem.name || '';
          this.settings[settingsItem.name].update = true;
        });
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas pobierania ustawień'});
      }
    );
  }

  /**
   * Save new settings or update the existing one
   *
   * @param {SettingsItem} settingsData Settings data of one item
   * @param {string} name Name of settings' category
   * @memberof SettingsComponent
   */
  save(settingsData: SettingsItem, name: string) {
    settingsData.name = name;
    if (settingsData.update) {
      this.settingsService.updateSettingsItem(settingsData).subscribe(
        settingsItem => {
          this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Ustawienia zostały zaktualizowane'});
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas aktualizacji ustawień'});
        }
      );
    } else {
      this.settingsService.saveSettingsItem(settingsData).subscribe(
        settingsItem => {
          this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Ustawienia zostały zapisane'});
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas zapisywania ustawień'});
        }
      );
    }
  }

}
