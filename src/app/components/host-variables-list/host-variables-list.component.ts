import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { HostService } from '../../services/host.service';
import { Host } from '../../interfaces/host';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';
import { DetectedLogEntry } from '../../interfaces/detected-log-entry';


/**
 * Displaying list of found entries in logs according to the rules
 *
 * @export
 * @class HostVariablesListComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-host-variables-list',
  templateUrl: './host-variables-list.component.html',
  styleUrls: ['./host-variables-list.component.scss']
})
export class HostVariablesListComponent implements OnInit {

  host: Host = {};

  constructor(private hostService: HostService, private route: ActivatedRoute, private messageService: MessageService) {
  }

 /**
  * Load and init host data based on route params using service
  *
  * @memberof HostVariablesListComponent
  */
 ngOnInit() {
    this.route.params.subscribe(params => {
      this.hostService.getHostById(params.id).subscribe(
        host => {
          this.host = host;
        },
        err => {
          this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas pobierania danych'});
        }
      );
    });
  }

}
