import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostVariablesListComponent } from './host-variables-list.component';

describe('HostVariablesListComponent', () => {
  let component: HostVariablesListComponent;
  let fixture: ComponentFixture<HostVariablesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostVariablesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostVariablesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
