import { Component, OnInit } from '@angular/core';
import { Log } from '../../interfaces/log';
import { LogService } from '../../services/log.service';
import { LazyLoadEvent } from 'primeng/api';
import { MessageService } from 'primeng/components/common/messageservice';

/**
 * Display all logs in table from database
 *
 * @export
 * @class LogListComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-log-list',
  templateUrl: './log-list.component.html',
  styleUrls: ['./log-list.component.scss']
})
export class LogListComponent implements OnInit {

  logs: Log[];

  totalRecords: Number;

  cols: any[];

  loading: boolean;

  constructor(private logService: LogService, private messageService: MessageService) { }

  /**
   * Load total number of logs in database.
   * Prepare headers for table
   *
   * @memberof LogListComponent
   */
  ngOnInit() {
    this.logService.getLogsCount().subscribe(
      total => {
        this.totalRecords = total,
        this.logService.getLogsBasicData(0, 10).subscribe(
          res => {
            this.logs = res;
          },
          err => {
          }
        );
      },
      err => console.log(err)
    );


    this.cols = [
      { field: '_id', header: 'logId'},
      { field: 'filename', header: 'Nazwa pliku'},
      { field: 'path', header: 'Ścieżka'},
      { field: 'size', header: 'Rozmiar'},
      { field: 'last_modified', header: 'Data ostatniej modyfikacji'},
      { field: 'update_at', header: 'Data ostatniego pobrania'},
      // { field: 'craete_at', header: 'Data utworzenia'}
    ];
  }

  /**
   * Lazy loading data for table based on the first element to fetch and rows number
   *
   * @param {LazyLoadEvent} event Event data
   * @memberof LogListComponent
   */
  loadLogsLazy(event: LazyLoadEvent) {
    this.logService.getLogsBasicData(event.first, event.rows).subscribe(
      res => {
        this.logs = res;
      },
      err => {
    });
  }

  /**
   * Remove selected log using service
   *
   * @param {Object} log Log model to remove
   * @memberof LogListComponent
   */
  removeLog(log) {
    this.logService.removeLog(log._id).subscribe(
      res => {
        const index = this.logs.indexOf(log);
        this.logs.splice(index, 1);
        this.messageService.add({severity: 'success', summary: 'Sukces!', detail: 'Wybrany log został usunięty'});
      },
      err => {
        this.messageService.add({severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas usuwania wybranego loga'});
      }
    );
  }

  /**
   * Remove all logs from database
   *
   * @memberof LogListComponent
   */
  deleteAllLogs() {
    this.logService.deleteAll().subscribe(
      res => {
        this.logs = [];
        this.totalRecords = 0;
        this.messageService.add({ severity: 'success', summary: 'Sukces!', detail: 'Logi zostały usunięte' });
      },
      err => {
        this.messageService.add({ severity: 'error', summary: 'Błąd!', detail: 'Wystąpił błąd podczas usuwania logów' });
      }
    );
  }

}
