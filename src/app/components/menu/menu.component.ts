import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

/**
 * Displaying menu on the page
 *
 * @export
 * @class MenuComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  items: MenuItem[];

  constructor() {}

  /**
   * Init routing paths
   */
  ngOnInit() {
    this.items = [
      {
        label: 'Hosty',
        routerLink: '/hosts'
      },
      {
        label: 'Logi',
        routerLink: '/logs'
      },
      {
        label: 'Reguły',
        routerLink: '/rules'
      },
      {
        label: 'Powiadomienia',
        routerLink: '/notifications'
      },
      {
        label: 'Wyniki',
        routerLink: '/notifications/results'
      }
    ];
  }

}
