import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenubarModule } from 'primeng/menubar';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {GrowlModule} from 'primeng/growl';
import {SplitButtonModule} from 'primeng/splitbutton';
import {CalendarModule} from 'primeng/calendar';
import {ToolbarModule} from 'primeng/toolbar';
import {MultiSelectModule} from 'primeng/multiselect';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import {FieldsetModule} from 'primeng/fieldset';

import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { appRoutes } from './routes';
import { HostService } from './services/host.service';
import { HostsListComponent } from './components/hosts-list/hosts-list.component';
import { MessageService } from 'primeng/components/common/messageservice';
import { LogListComponent } from './components/log-list/log-list.component';
import { LogService } from './services/log.service';
import { RulesListComponent } from './components/rules-list/rules-list.component';
import { RulesService } from './services/rules.service';
import { RuleFormComponent } from './components/rule-form/rule-form.component';
import { HostVariablesListComponent } from './components/host-variables-list/host-variables-list.component';
import { NotificationsListComponent } from './components/notifications-list/notifications-list.component';
import { NotificationFormComponent } from './components/notification-form/notification-form.component';
import { NotificationsService } from './services/notifications.service';
import { SettingsComponent } from './components/settings/settings.component';
import { SettingsService } from './services/settings.service';
import { NotificationResultsComponent } from './components/notification-results/notification-results.component';


/**
 * @ignore
 *
 * @export
 * @class AppModule
 */
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HostsListComponent,
    LogListComponent,
    RulesListComponent,
    RuleFormComponent,
    HostVariablesListComponent,
    NotificationsListComponent,
    NotificationFormComponent,
    SettingsComponent,
    NotificationResultsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule,
    HttpClientModule,
    MenubarModule,
    DropdownModule,
    InputTextareaModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    DialogModule,
    GrowlModule,
    SplitButtonModule,
    CalendarModule,
    ToolbarModule,
    MultiSelectModule,
    AmazingTimePickerModule,
    FieldsetModule
  ],
  providers: [HostService, MessageService, LogService, RulesService, NotificationsService, SettingsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
