import { Time } from '@angular/common/src/i18n/locale_data_api';

export interface Rule {
  regExp?: String;
  startTime?: Time;
  stopTime?: Time;
  value?: String;
}
