import { HostRulesSet } from './host-rules-set';

/**
 * Model of path in host.
 * It contains pattern of the date-time in the file to find it in a content and date-time format.
 *
 * @export
 * @interface HostPath
 */
export interface HostPath {
  path?: String;
  pattern?: String;
  datetimePatter?: String;
}

/**
 * Model of host
 */
export interface Host {
    _id?: String;
    name?: String;
    address?: String;
    port?: Number;
    protocolType?: String;
    paths?: HostPath[];
    login?: String;
    password?: String;
}
