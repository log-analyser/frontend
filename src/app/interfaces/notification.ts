import { DetectedLogEntry } from './detected-log-entry';
import { Variable } from './variable';
import { Host } from './host';
import { RulesSet } from './rules-set';
import { NotificationRulesSet } from './notification-rules-set';
import { NotificationVariable } from './notification-variable';

export interface Notification {
  _id?: String;
  hostId?: Host;
  files?: [DetectedLogEntry];
  description?: String;
  email?: String;
  requiredVariablesSets?: NotificationVariable[][];
  date?: Date;
  schedule?: String;
  rulesSets?: NotificationRulesSet[]|any;
  inProgress?: boolean;
}
