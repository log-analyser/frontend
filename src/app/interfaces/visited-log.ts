import { Log } from './log';
export interface VisitedLog {
  log: Log;
  lineNumber: Number;
}
