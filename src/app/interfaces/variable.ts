import { Rule } from './rule';
export interface Variable {
  name?: String;
  rules?: Rule[];
}
