import { DetectedLogEntry } from './detected-log-entry';

/**
 * Model of variable from rules' set connected with host
 * It contains list of files
 *
 * @export
 * @interface HostVariable
 */
export interface HostVariable {
  name?: String;
  value?: String;
  files?: [DetectedLogEntry];
}
