import { HostVariable } from './host-variable';

/**
 * Model of the variables and rules' set connected with the host
 *
 * @export
 * @interface HostRulesSet
 */
export interface HostRulesSet {
  ruleSetId?: String;
  variables?: HostVariable[];
}
