import { DetectedLogEntry } from './detected-log-entry';

/**
 * Model of the files found by the one set fo rules
 *
 * @export
 * @interface DetectedFiles
 */
export interface DetectedFiles {
  rulesSetId: String;
  files: DetectedLogEntry[];
}
