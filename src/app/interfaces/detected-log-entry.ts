/**
 * Model of the found entry in one log file
 *
 * @export
 * @interface DetectedLogEntry
 */
export interface DetectedLogEntry {
  filename?: String;
  path?: String;
  lineNumber?: Number;
  line?: String;
  date?: Date;
}
