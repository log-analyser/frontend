export interface RuleResponse {
  hostId?: String;
  path?: String;
  filename?: String;
  ruleId?: String;
}
