export interface SettingsItem {
  _id?: String;
  name?: string;
  data?: Object;
  update?: boolean;
}

export interface Settings {
  [key: string]: SettingsItem;
}
