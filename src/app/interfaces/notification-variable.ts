export interface NotificationVariable {
  name?: String;
  value?: String;
  _id?: String;
  nextVariables?: [NotificationVariable];
}
