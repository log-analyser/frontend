import { Variable } from './variable';
export interface RulesSet {
  _id?: String;
  name?: String;
  variables?: Variable[];
}
