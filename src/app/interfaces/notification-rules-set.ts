import { RulesSet } from './rules-set';
import { DetectedLogEntry } from './detected-log-entry';
import { NotificationVariable } from './notification-variable';

export interface NotificationRulesSet {
  rulesSet: RulesSet;
  files: DetectedLogEntry[];
  variables?: NotificationVariable[];
}
