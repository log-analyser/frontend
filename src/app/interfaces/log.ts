/**
 * Model of log
 *
 * @export
 * @interface Log
 */
export interface Log {
  _id?: String;
  filename?: String;
  path?: String;
  hostId?: String;
  content?: String;
  size?: Number;
  last_modified?: Date;
  update_at?: Date;
  craete_at?: Date;
}
