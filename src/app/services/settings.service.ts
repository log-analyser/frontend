import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SettingsItem } from '../interfaces/settings-item';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../app.config';

@Injectable()
export class SettingsService {

  constructor(private http: HttpClient) { }

  getSettings(): Observable<SettingsItem[]> {
    return this.http.get<SettingsItem[]>(AppConfig.API_ENDPOINT + '/settings');
  }

  saveSettingsItem(settingsItem: SettingsItem): Observable<SettingsItem> {
    return this.http.post<SettingsItem>(AppConfig.API_ENDPOINT + '/setting', settingsItem);
  }

  updateSettingsItem(settingsItem: SettingsItem): Observable<SettingsItem> {
    return this.http.put<SettingsItem>(AppConfig.API_ENDPOINT + `/setting/${settingsItem._id}`, settingsItem);
  }
}
