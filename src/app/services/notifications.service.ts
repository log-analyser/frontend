import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { Notification } from './../interfaces/notification';

@Injectable()
export class NotificationsService {
  selectedNotification: Notification;

  constructor(private http: HttpClient) { }

  saveNotification(notification: Notification): Observable<Notification> {
    return this.http.post<Notification>(AppConfig.API_ENDPOINT + '/notification', notification);
  }

  getAllNotifications(): Observable<Notification[]> {
    return this.http.get<Notification[]>(AppConfig.API_ENDPOINT + '/notifications');
  }

  deleteNotification(notification: Notification): Observable<Notification> {
    return this.http.delete<Notification>(AppConfig.API_ENDPOINT + `/notification/${notification._id}`);
  }

  updateNotification(notification: Notification): Observable<Notification> {
    return this.http.put<Notification>(AppConfig.API_ENDPOINT + `/notification/${notification._id}`, notification);
  }

  runNotification(notification: Notification): Observable<Notification> {
    return this.http.get<Notification>(AppConfig.API_ENDPOINT + `/notification/${notification._id}/run`);
  }

  scheduleNotification(notification: Notification): Observable<any> {
    return this.http.get<any>(AppConfig.API_ENDPOINT + `/notification/${notification._id}/schedule`);
  }

  stopNotification(notification: Notification): Observable<any> {
    return this.http.get<any>(AppConfig.API_ENDPOINT + `/notification/${notification._id}/stop`);
  }

}
