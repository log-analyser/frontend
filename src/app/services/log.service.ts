import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Log } from '../interfaces/log';
import { AppConfig } from '../app.config';

@Injectable()
export class LogService {

  constructor(private http: HttpClient) { }

  getLogsCount(): Observable<Number> {
    return this.http.get<Number>(AppConfig.API_ENDPOINT + '/logs/count');
  }

  getLogsBasicData(skip, limit): Observable<Log[]> {
    return this.http.get<Log[]>(AppConfig.API_ENDPOINT + '/logs', {params: {limit: limit, skip: skip}});
  }

  getLogContent(id) {
    return this.http.get(AppConfig.API_ENDPOINT + `/logs/${id}/content`);
  }

  removeLog(id) {
    return this.http.delete(AppConfig.API_ENDPOINT + `/log/${id}`);
  }

  deleteAll() {
    return this.http.delete(AppConfig.API_ENDPOINT + `/logs`);
  }

}
