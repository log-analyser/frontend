import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Host } from '../interfaces/host';

@Injectable()
export class HostService {

  constructor(private http: HttpClient) { }

  postHost(data) {
    return this.http.post(AppConfig.API_ENDPOINT + '/host', data);
  }

  updateHost(host: Host) {
    return this.http.put(AppConfig.API_ENDPOINT + '/host/' + host._id, host);
  }

  getHosts(): Observable<Host[]> {
    return this.http.get<Host[]>(AppConfig.API_ENDPOINT + '/hosts');
  }

  getHostById(id): Observable<Host> {
    return this.http.get<Host>(AppConfig.API_ENDPOINT + `/hosts/${id}`);
  }

  downloadLogs(hostId) {
    return this.http.get(AppConfig.API_ENDPOINT + '/logs/' + hostId);
  }

  deleteHost(hostId) {
    return this.http.delete(AppConfig.API_ENDPOINT + '/host/' + hostId);
  }

}
