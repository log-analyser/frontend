import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { Rule } from '../interfaces/rule';
import { Observable } from 'rxjs/Observable';
import { RuleResponse } from '../interfaces/rule-response';
import { RulesSet } from '../interfaces/rules-set';

@Injectable()
export class RulesService {
  selectedRuleSet: RulesSet = null;


  getSelectedRuleSet() {
    return this.selectedRuleSet;
  }

  setSelectedRuleSet(ruleSet: RulesSet) {
    this.selectedRuleSet = ruleSet;
  }

  constructor(private http: HttpClient) { }

  saveRulesSet(rulesSet: RulesSet): Observable<RulesSet> {
    return this.http.post<RulesSet>(AppConfig.API_ENDPOINT + `/rulesset`, rulesSet);
  }

  updateRulesSet(rulesSet: RulesSet): Observable<RulesSet> {
    return this.http.put<RulesSet>(AppConfig.API_ENDPOINT + `/rulesset/${rulesSet._id}`, rulesSet);
  }

  getRulesSets(): Observable<RulesSet[]> {
    return this.http.get<RulesSet[]>(AppConfig.API_ENDPOINT + `/rulessets`);
  }

  deleteRulesSet(rulesSet): Observable<RulesSet> {
    return this.http.delete<RulesSet>(AppConfig.API_ENDPOINT + `/rulesset/${rulesSet._id}`);
  }

  runRules(hostId): Observable<RuleResponse[]> {
    return this.http.get<RuleResponse[]>(AppConfig.API_ENDPOINT + `/rulessets/${hostId}/run`);
  }

  runRule(hostId, ruleId): Observable<RuleResponse> {
    return this.http.get<RuleResponse>(AppConfig.API_ENDPOINT + `/rulessets/${hostId}/run/${ruleId}`);
  }
}
